package slogger

import (
	"fmt"
	"path"
	"sync"
)

const (
	logFormat = "| %s | %s"
	infoLogName = "info.log"
	errorLogName = "error.log"
	debugLogName = "debug.log"
)

var customLoggers map[string]*logger
var logPath = "/tmp/"

var infoOnce sync.Once
var infoLogger logger

var errorOnce sync.Once
var errorLogger logger

var debugOnce sync.Once
var debugLogger logger

func getInfoInstance() logger {
	infoOnce.Do(func() {
		infoLogger = logger{}
		infoLogger.SetLogPath(path.Join(logPath, infoLogName))
		infoLogger.logToScreen = true
		infoLogger.GetInstance()
	})
	return infoLogger
}

func getErrorInstance() logger {
	errorOnce.Do(func() {
		errorLogger = logger{}
		errorLogger.SetLogPath(path.Join(logPath, errorLogName))
		errorLogger.logToScreen = true
		errorLogger.GetInstance()
	})
	return errorLogger
}

func getDebugInstance() logger {
	debugOnce.Do(func() {
		debugLogger = logger{}
		debugLogger.SetLogPath(path.Join(logPath, debugLogName))
		debugLogger.logToScreen = true
		debugLogger.GetInstance()
	})
	return debugLogger
}

func getCustomInstance(logName string, stdout bool) *logger {
	if customLoggers == nil {
		customLoggers = make(map[string]*logger)
	}
	if customLogger, exists := customLoggers[logName]; exists {
		return customLogger
	} else {
		tempLogger := logger{}
		tempLogger.SetLogPath(path.Join(logPath, logName))
		tempLogger.SetStdOut(stdout)
		tempLogger.GetInstance()
		customLoggers[logName] = &tempLogger
		return customLoggers[logName]
	}
}

func logWithPrefix(prefix string, loggerInstance *logger, v ...interface{}) {
	loggerInstance.Println(fmt.Sprintf(logFormat, prefix, v))
}

func Info(v ...interface{}) {
	i := getInfoInstance()
	logWithPrefix("INFO", &i, v)
}


func Error(v ...interface{}) {
	e := getErrorInstance()
	logWithPrefix("ERROR", &e, v)
}

func Debug(v ...interface{}) {
	d := getDebugInstance()
	logWithPrefix("DEBUG", &d, v)
}

func Custom(logName string, stdout bool, v ...interface{}) {
	c := getCustomInstance(logName, stdout)
	logWithPrefix(logName, c, v)
}

func SetLogPath(path string) {
	logPath = path
}


