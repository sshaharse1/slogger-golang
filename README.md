This library implements a very simple approach to logging to a file and stdout.


It provides the functions Info,Debug and Error which just log both to a file and stdout.
The function SetLogPath sets the file path. You can use it once in any scope and it will apply to ALL scopes.

