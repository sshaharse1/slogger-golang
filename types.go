package slogger

import (
	"io"
	"log"
	"os"
	"sync"
)


type logger struct {
	LogPath string
	logToScreen bool
	log *log.Logger
	once sync.Once
}

func (l *logger) GetInstance() *log.Logger {
	l.once.Do(func() {
		l.log = l.createLogger()
	})
	return l.log
}

func (l logger) createLogger() *log.Logger {
	logger := log.New(l.getWriter(), "", log.Ldate|log.Ltime)
	return logger
}

func (l logger) getWriter() io.Writer {
	logFile, err := os.OpenFile(l.LogPath, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0777)
	if err != nil {
		panic(err)
	}
	if l.logToScreen {
		return io.MultiWriter(os.Stdout, logFile)
	} else {
		return logFile
	}
}

func (l logger) Println(v ...interface{}) {
	l.log.Println(v)
}

func (l *logger) SetLogPath(logPath string) {
	l.LogPath = logPath
}

func (l *logger) SetStdOut(stdout bool) {
	l.logToScreen = stdout
}
